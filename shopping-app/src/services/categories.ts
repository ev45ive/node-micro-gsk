import { CategoryCreatePayload } from "../interfaces/categories"
import { Category } from "../db/models/category"
import {NotFoundError} from "../middlewares/errors";

export const getCategories = () => {
  return Category.findAll()
}

export const getCategoryById = (id: string) => {
  return Category.findByPk(id)
}

export const createCategory = (categoryPayload: CategoryCreatePayload) => {
  const c = new Category();

  c.name = categoryPayload.name || ''
  c.desc = categoryPayload.desc || ''

  if (categoryPayload.parentId) {
    c.parentId = categoryPayload.parentId;
  }

  return c.save()
}

export const getCategoriesByParent = (parentId: string) => {
  return Category.findAll({
    where: {
      parentId: parentId
    }
  })
}

export const updateCategoryById = async (data) => {
  const c = await Category.findByPk(data.id)

  if (!c) {
    throw new NotFoundError('Category not found')
  }

  data.name && c.set('name', data.name)
  data.desc && c.set('desc', data.desc)
  data.parentId && c.set('parentId', data.parentId)

  return (c.save())
}
