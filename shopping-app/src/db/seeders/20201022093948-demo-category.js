'use strict';
const uuid = require('uuid-by-string')

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    return queryInterface.bulkInsert('Categories', [
      {
        id: uuid('odzież'),
        name: 'odzież',
        desc: 'odzież desc',
        createdAt: '2020-10-22T08:10:48.742Z',
        updatedAt: '2020-10-22T08:10:48.742Z',
      },
      {
        id: uuid('obuwie'),
        name: 'obuwie',
        desc: 'obuwie desc',
        createdAt: '2020-10-22T08:10:48.742Z',
        updatedAt: '2020-10-22T08:10:48.742Z',
      },
      {
        id: uuid('sukienki'),
        name: 'sukienki',
        desc: 'sukienki desc',
        parentId: uuid('odzież'),
        createdAt: '2020-10-22T08:10:48.742Z',
        updatedAt: '2020-10-22T08:10:48.742Z',
      },
      {
        id: uuid('płaszcze'),
        name: 'płaszcze',
        desc: 'płaszcze desc',
        parentId: uuid('odzież'),
        createdAt: '2020-10-22T08:10:48.742Z',
        updatedAt: '2020-10-22T08:10:48.742Z',
      },
      {
        id: uuid('klapki'),
        name: 'klapki',
        desc: 'klapki desc',
        parentId: uuid('obuwie'),
        createdAt: '2020-10-22T08:10:48.742Z',
        updatedAt: '2020-10-22T08:10:48.742Z',
      },
    ])
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    return  queryInterface.bulkDelete('Categories', null, {});

  }
};
