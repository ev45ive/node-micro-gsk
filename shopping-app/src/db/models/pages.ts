'use strict';
import {DataTypes, Model, Optional,  UUIDV4} from 'sequelize';
import {sequelize} from "../index";

// These are all the attributes in the User model
interface PageAttributes {
    id: string;
    title: string;
    createdAt?: string
    updatedAt?: string
}

// Some attributes are optional in `User.build` and `User.create` calls
interface PageCreationAttributes extends Optional<PageAttributes, "id"> { }

// export class Pages extends Model<PagesAttributes, UserCreationAttributes> implements PagesAttributes {
export class Page extends Model<PageAttributes> implements PageAttributes {
    id!: string;
    title: string;
}

Page.init({
    id: {
        primaryKey: true,
        type: DataTypes.UUID,
        defaultValue: UUIDV4 // Or Sequelize.UUIDV1
    },
    // Model attributes are defined here
    title: {
        type: DataTypes.STRING,
        allowNull: false
    },
}, {
    // Other model options go here
    sequelize, // We need to pass the connection instance
    modelName: 'Pages', // We need to choose the model name
    // don't forget to enable timestamps!
    timestamps: true, // createdAt, updatedAt
});
// const {
//   Model
// } = require('sequelize');
// module.exports = (sequelize, DataTypes) => {
//   class Pages extends Model {
//     /**
//      * Helper method for defining associations.
//      * This method is not a part of Sequelize lifecycle.
//      * The `models/index` file will call this method automatically.
//      */
//     static associate(models) {
//       // define association here
//     }
//   };
//   Pages.init({
//     title: DataTypes.STRING
//   }, {
//     sequelize,
//     modelName: 'Pages',
//   });
//   return Pages;
// };
