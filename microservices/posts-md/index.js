const express = require('express')
const client = require('axios')

const server = express()
server.use(express.json())

const posts = [
  { id: 123, name: 'Post 123' }
]

server.get('/posts', (req, res) => { res.send(posts) })

server.get('/posts/:id', (req, res) => {
  const id = req.params.id
  res.send(posts.find(p => p.id == id))
})

server.post('/posts', async (req, res) => {
  const post = { ...req.body }
  post.id = Date.now()

  posts.push(post)

  await publishEvent('posts:created', post)

  res.send(post)
})


server.put('/posts/:id', async (req, res) => {
  const post = { ...req.body }
  const existing = post.find(p => p.id == post.id)
  existing.name = post.name

  await publishEvent('posts:updated', post)

  res.send(existing)
})

server.delete('/posts/:post_id', async (req, res) => {
  const post_id = req.params['post_id']
  const index = posts.findIndex(p => p.id == post_id)
  delete posts[index]

  await publishEvent('posts:deleted', post)

  res.send({ ok: 1 })
})

server.listen(9001, () => console.log('Comments listening on 9001'))


function publishEvent(topic, data) {
  return client.post('http://localhost:9002/events/', {
    topic: topic, data
  })
}