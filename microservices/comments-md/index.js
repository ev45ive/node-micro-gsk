const express = require('express')
const client = require('axios')

const server = express()
server.use(express.json())

const comments = [
  { id: 123, name: 'comment 123', postId: 123 }
]
const postsExist = { 456: true }

server.get('/comments', (req, res) => { res.send(comments) })
server.get('/posts/:post_id/comments', (req, res) => { res.send(comments) })

server.post('/posts/:post_id/comments', async (req, res) => {
  const post_id = req.params['post_id']

  if (!postsExist[post_id]) { res.send('Post does not exist'); return; }

  const comment = { ...req.body }
  comment.id = Date.now()

  comments.push(req.body)
  res.send(comment)
})

server.post('/events', (req, res) => {
  const { topic, data: eventData } = req.body;
  
  console.log('Recieved event '+topic, eventData)
  switch (topic) {
    case 'posts:created': {
      postsExist[eventData.id] = true
    } break;
    // case 'posts:updated': {
    //   postsExist[eventData.id] == true
    // } break;
    case 'posts:deleted': {
      postsExist[eventData.id] = false
    } break;
  }
  res.send({ ok: 1 })
})

  ; (async () => {
    console.log('Syncing posts...')
    const { data: posts } = await client.get('http://localhost:9001/posts/')
    console.log(posts)

    posts.forEach(post => {
      postsExist[post.id] = true
    });

    server.listen(9002, () => console.log('Comments listening on 9002'))
  })()
